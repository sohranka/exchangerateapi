ccy==1.2.0
geocoder==1.38.1
requests==2.24.0

pytest==6.1.1

fastapi==0.61.1
APScheduler==3.6.3
